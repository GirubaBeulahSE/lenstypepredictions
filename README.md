#Author: Giruba Beulah SE
# A Simple Machine Learning .NET Application that uses the lenses dataset available in UCI Repository to
#predict the type of lens for a patient

#The actual dataset is not comma separated.
#For implementation purposes, the dataset was made comma separated.